本文主要为您介绍如何在kata环境中基于海光安全加密虚拟化功能CSV(China Secure Virtualization)技术，通过runtime-attestaion 认证方式，启动一个租户的加密容器镜像。

# 前提条件

请使用安装Hygon CPU的硬件设备，硬件信息参考如下：

- CPU型号：Hygon C86 7291 32-core Processor
- 固件版本：1600及以上
- BIOS设置：开启SME

BIOS选项SMEE用来控制是否打开内存加密功能，SMEE=Enable表示在BIOS中打开内存加密功能，SMEE=Disable表示在BIOS中关闭内存加密功能。

## 1. 安装Anolis 8.4 操作系统

请参考[Anolis 8.4 GA说明文档](https://mirrors.openanolis.cn/anolis/8.4/isos/GA/ReadMe.txt)安装anolis 8.4 GA。

## 2. 升级kernel到5.10

Anlois 8.4 的默认内核版本是4.19，5.10的内核上支持[CSV远程证明功能](https://gitee.com/anolis/cloud-kernel/pulls/14)。请升级kernel 到5.10版本。

1. 请参考以下命令，添加Anolis的Experimental repo，并将kernel升级至5.10。
```
yum-config-manager --add-repo https://mirrors.openanolis.cn/anolis/8/Experimental/x86_64/os/ && \
  yum update kernel
```
2. 配置bootloader。
```
grubby --update-kernel=ALL --args="mem_encrypt=on kvm_amd.sev=1"
```
3. 重启机器，请输入以下命令查看内核版本。
```shell
uname -r
```
预期输出：
```shell
5.10.134-12.an8.x86_64
```

**注意！！**  

如果您使用的是Anolis 8.6 GA镜像，可能会碰到使能SEV之后，机器Hang住无法进入系统的情况。请参考以下步骤降级grub2-efi之后，可以正常启动这个特性

```sh
yum downgrade grub2-efi
```

## 3. 检查CSV使能状态 

1.  在操作系统内执行：
```
dmesg | grep -i sev
```

下图表示CSV已经使能。 

![](https://oss.openanolis.cn/sig/jyxnkmbnxviifztgmeep)

2.  检查kvm_amd和ccp模块是否成功安装。
```
lsmod | grep kvm
```
下图表示成功安装。 

![](https://oss.openanolis.cn/sig/ffhuletbduwrkhkkgaih)

## 4. 使用hag检查固件版本信息

1. 安装hag

```sh
yum-config-manager --add-repo https://mirrors.openanolis.org/inclavare-containers/anolis8.4 && \
 rpm --import https://mirrors.openanolis.org/inclavare-containers/anolis8.4/RPM-GPG-KEY-rpm-sign && \
 yum install -y hag
```

2. 通过hag获得平台状态

```sh
sudo hag --platform_status
api_major:          1
api_minor:          3
platform_state:     CSV_STATE_WORKING
owner:              PLATFORM_STATE_SELF_OWN
chip_secure:        SECURE
fw_enc:             ENCRYPTED
fw_sign:            SIGNED
es:                 CSV ES
build id:           1644
bootloader version: 0.0.0
guest_count:        1
supported csv guest:11
platform_status command successful

```

注意：固件 build id 要大于等于 1600 才可以支持远程证明。

## 5. 执行CSV 检查脚本，检查环境是否满足 (可选)
```sh
./check_csv_env.sh
```

脚本内容见附录

# 背景信息

![](https://oss.openanolis.cn/sig/mftnpcpuvawveyodvhtn)

1. CSV VM启动；

2. 下载加密镜像时才会通过attestation-agent将通过vm-attestation hypercall获取的包括attestation-report 、chip-id等内容的CSV VM evidence发送给verdictd server校验；

3. 校验通过后virdictd才与attestation-agent建立基于rats-tls的可信硬件环境的安全通道、并将加密镜像的解密key通过该安全通道发送给attestation-agent；

4. CSV VM利用步骤3获得的解密key解密镜像，运行工作负载

# 步骤一：配置权限
### 1. 关闭firewall
 Linux系统下面自带了防火墙iptables，iptables可以设置很多安全规则。但是如果配置错误很容易导致各种网络问题。此处建议关闭firewall。
执行如下操作：
```
sudo service firewalld stop
```
执行完毕后结果应类似如下：

![](https://oss.openanolis.cn/sig/eiaokzmkrqohrzcldbyh)

### 2. 关闭selinux
 Security-Enhanced Linux（SELinux）是一个在內核中实施的强制存取控制（MAC）安全性机制。  
为避免出现权限控制导致的虚拟机启动、访问失败等问题，此处建议关闭selinux。
执行如下操作：
```
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```
执行成功后：
使用getenforce检查，结果应类似如下：

![](https://oss.openanolis.cn/sig/wgkycimdpbhbeewmiqld)

# 步骤二：安装kata 环境

Kata Containers是一个开源的、致力于用轻量级虚拟机构建一个安全的容器运行时的实现，这些虚拟机在感觉和执行上与容器类似，但使用硬件虚拟化技术作为第二层防御，提供了更强的工作负载隔离。

关于项目的更多信息，请参见[kata-container](https://github.com/confidential-containers/kata-containers-CCv0)。

## 1. 安装kata-containers

1. 请执行以下命令，安装kata-containers。
```shell
yum install -y kata-static
```

2. 运行以下命令，查看kata-containers是否安装成功。
```shell
tree /opt/kata/
```

返回结果示例如下，表示已安装成功。

![](https://oss.openanolis.cn/sig/bdpavhcztunbimlzvnuz)

## 2. 安装qemu
此处使用的qemu基于6.2.0构建。
```shell
yum install -y qemu-system-x86_64
```
## 3. 安装guest kernel，initrd，ovmf
ccv0-guest中包含kata运行CSV VM所需的guest kernel、initrd、OVMF、cmdline等文件。
其中：
guest的rootfs和kernel，需使用efi_secret的内核模块以支持向文件系统中注入secret，加入AA并修改AA设置，自行构建请参考[guest Rootfs and Kernel](https://github.com/confidential-containers/documentation/blob/main/demos/sev-demo/README.md#rootfs-and-kernel) ；
这里提供的OVMF是基于f0f3f5aae7c4d346ea5e24970936d80dc5b60657 进行构建的，也可以使用[edk2-stable202108](https://github.com/tianocore/edk2/releases/tag/edk2-stable202108)后的版本自行构建，以支持CSV。

```shell
yum install -y ccv0-guest
```

cmdline中记录了CSV VM启动时所需的参数信息，需根据实际使用情况进行修改。可参考以下命令：
```sh
cat <<EOF | sudo tee /opt/csv/ccv0-guest/cmdline
tsc=reliable no_timer_check rcupdate.rcu_expedited=1 i8042.direct=1 i8042.dumbkbd=1 i8042.nopnp=1 i8042.noaux=1 noreplace-smp reboot=k console=hvc0 console=hvc1 cryptomgr.notests net.ifnames=0 pci=lastbus=0 quiet panic=1 nr_cpus=`cat /proc/cpuinfo| grep processor | wc -l` scsi_mod.scan=none agent.config_file=/etc/agent-config.toml
EOF
```
## 4. 安装kata-runtime

kata-runtime运行CSV VM。

```
yum -y install kata-runtime
```
## 5. 配置kata-runtime
执行以下命令，配置kata 运行时：
这里修改了kata-runtime默认配置中的qemu、guest kernel && initrd && OVMF路径；
使能confidential-guest选项并加入attestation-agent-config配置；
将默认内存大小由2048调整为8000；
将共享文件系统由"virtio-fs"调整为"virtio-9p"。
```
mkdir -p /etc/kata-containers/ && \
cp /opt/kata/share/defaults/kata-containers/configuration.toml /etc/kata-containers/ && \
cd /etc/kata-containers/ && \
sed -i 's/opt\/kata\/bin\/qemu-system-x86_64/opt\/qemu\/bin\/qemu-system-x86_64/' configuration.toml && \
sed -i 's/kata\/share\/kata-containers\/vmlinux.container/csv\/ccv0-guest\/vmlinuz-5.15.0-rc5+/' configuration.toml && \
sed -i 's/image = \"\/opt\/kata\/share\/kata-containers\/kata-containers/initrd = \"\/opt\/csv\/ccv0-guest\/initrd.run/' configuration.toml && \
sed -i 's/\# confidential_guest/confidential_guest/' configuration.toml && \
sed -i 's/kernel_params = \"\"/kernel_params = \"agent.config_file=\/etc\/agent-config.toml\"/' configuration.toml && \
sed -i 's/firmware = \"\"/firmware = \"\/opt\/csv\/ccv0-guest\/OVMF.fd\"/' configuration.toml && \
sed -i 's/default_memory = 2048/default_memory = 8000/' configuration.toml && \
sed -i 's/shared_fs = \"virtio-fs\"/shared_fs = \"virtio-9p\"/' configuration.toml && \
sed -i 's/\#service_offload/service_offload/' configuration.toml
```
# 步骤三：安装containerd
Containerd是一个行业标准的容器运行时，强调简单性、健壮性和可移植性。它可以作为Linux和Windows的守护进程，可以管理其主机系统的完整容器生命周期:图像传输和存储、容器执行和监督、底层存储和网络附件等。
更多信息请参考[containerd](https://github.com/containerd/containerd)

1. 执行以下命令，安装containerd
```shell
sudo yum install -y containerd
```

2. 启动containerd

```shell
sudo systemctl enable /etc/systemd/system/containerd.service
sudo systemctl daemon-reload
sudo service containerd restart
```

预期输出类似如下：
```
● containerd.service - containerd container runtime
   Loaded: loaded (/etc/systemd/system/containerd.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2022-03-31 16:52:43 CST; 1s ago
     Docs: https://containerd.io
  Process: 1884520 ExecStartPre=/sbin/modprobe overlay (code=exited, status=0/SUCCESS)
 Main PID: 1884522 (containerd)
    Tasks: 34
   Memory: 46.8M
   CGroup: /system.slice/containerd.service
           └─1884522 /usr/bin/containerd
```

# 步骤四：搭建kubenetes运行环境

Kubernetes，也称为K8s，是一个用于管理跨多个主机的容器化应用程序的开源系统。它为应用程序的部署、维护和扩展提供了基本机制。
更多信息请参考 [kubernetes](https://github.com/kubernetes/kubernetes) 项目介绍。
## 1.  系统环境配置
- 确保 br_netfilter 模块被加载

```shell
modprobe br_netfilter
```

- 允许 iptables 检查桥接流量

```shell
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```

- 关闭swap

```shell
swapoff –a
```

## 2. 安装 kubeadm、kubelet 和 kubectl

- 配置repo

```shell
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
```

- 设置服务文件

```shell
sudo mkdir -p  /etc/systemd/system/kubelet.service.d/
cat <<EOF | sudo tee  /etc/systemd/system/kubelet.service.d/0-containerd.conf
[Service]
Environment="KUBELET_EXTRA_ARGS=--container-runtime=remote --runtime-request-timeout=15m --container-runtime-endpoint=unix:///run/containerd/containerd.sock"
EOF

cat << EOF | sudo tee /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf
[Service]
Environment="KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf"
Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"

#This is a file that "kubeadm init" and "kubeadm join" generates at runtime, populating the KUBELET_KUBEADM_ARGS variable dynamically

EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env

#This is a file that the user can use for overrides of the kubelet args as a last resort. Preferably, the user should use
#the .NodeRegistration.KubeletExtraArgs object in the configuration files instead. KUBELET_EXTRA_ARGS should be sourced from this file.

EnvironmentFile=-/etc/sysconfig/kubelet
ExecStart=
ExecStart=/usr/bin/kubelet \$KUBELET_KUBECONFIG_ARGS \$KUBELET_CONFIG_ARGS \$KUBELET_KUBEADM_ARGS \$KUBELET_EXTRA_ARGS
EOF
systemctl enable kubelet.service
```
## 3. 初始化k8s 环境
```shell
sudo kubeadm init --cri-socket /run/containerd/containerd.sock --pod-network-cidr=10.244.0.0/16 --image-repository registry.aliyuncs.com/google_containers

mkdir -p $HOME/.kube
sudo /bin/cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
## 4. 允许pod使用master：
默认情况下，集群不会在主节点中调度 Pod。 要启用主节点调度：
```shell
su
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
```
## 5. 安装flannel网络
[Flannel](https://github.com/flannel-io/flannel#flannel) 是一个非常简单的能够满足 Kubernetes 所需要的覆盖网络。   Flannel使用Kubernetes或etcd来存储网络配置、分配的子网和主机公共IP等信息。数据包则通过VXLAN、UDP或host-gw这些类型的后端机制进行转发。  
```shell
wget --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 --tries=0 https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml  -O $HOME/kube-flannel.yml

kubectl apply -f $HOME/kube-flannel.yml
```
## 6. 检查k8s环境的状态
```shell
kubectl get pods --all-namespaces
```
预期输出：


![](https://oss.openanolis.cn/sig/teaorurqmoroqdgibwnj)


# 步骤五：安装并启动verdictd

Verdictd是一种远程认证实现，由一组构建块组成，这些构建块利用Intel/AMD的安全特性来发现、验证和支持关键的基础安全和机密计算用例。它依靠RATS-TLS应用远程认证基础和标准规范来维护平台数据收集服务和高效的验证引擎来执行全面的信任评估。这些信任评估可用于管理应用于任何给定工作负载的不同信任和安全策略。
更多信息请参考[verdictd](https://github.com/inclavare-containers/verdictd)项目文档。
## 1. 请执行以下命令，安装verdictd

```shell
yum install -y verdictd
```

## 2. 配置CSV OPA文件
使用/opt/csv/calculate_hash.py计算measure

```sh
yum install -y gop 
/opt/csv/calculate_hash.py --ovmf  /opt/csv/ccv0-guest/OVMF.fd --kernel /opt/csv/ccv0-guest/vmlinuz-5.15.0-rc5+  --initrd /opt/csv/ccv0-guest/initrd.run.img --cmdline /opt/csv/ccv0-guest/cmdline
```

输出结果类似如下：
```sh
Calculating hash of kernel at /opt/csv/ccv0-guest/vmlinuz-5.15.0-rc5+
Calculating hash of initrd at /opt/csv/ccv0-guest/initrd.run.img
Calculating hash of kernel params (/opt/csv/ccv0-guest/cmdline)
Firmware Digest:  OJXIhq3PHbknNmpAIy8YpUHOpY0wvGRXULOW8djVAZA=
```


配置csvData
```sh
mkdir -p /opt/verdictd/opa/ && cat <<EOF | sudo tee /opt/verdictd/opa/csvData
{
    "measure": ["OJXIhq3PHbknNmpAIy8YpUHOpY0wvGRXULOW8djVAZA="]
}
EOF
```

配置csvPolicy.rego
```sh
cat <<EOF | sudo tee /opt/verdictd/opa/csvPolicy.rego

package policy

# By default, deny requests.
default allow = false

allow {
    measure_is_grant
}


measure_is_grant {
    count(data.measure) == 0
}

measure_is_grant {
    count(data.measure) > 0
    input.measure == data.measure[_]
}
EOF
```

# 步骤六：制作加密镜像

## 1. 制作加密镜像

可参考[Generate encrypted container image](https://github.com/inclavare-containers/verdictd#generate-encrypted-container-image)制作加密镜像。
在本例中以 docker.io/zhouliang121/alpine-84688df7-2c0c-40fa-956b-29d8e74d16c1-gcm:latest 为例进行测试。

## 2. 部署镜像密钥

```
mkdir -p /opt/verdictd/keys/ && echo 1111111111111111111111111111111 > /opt/verdictd/keys/84688df7-2c0c-40fa-956b-29d8e74d16c1
```

## 3. 修改镜像policy
以docker.io/zhouliang121/alpine-84688df7-2c0c-40fa-956b-29d8e74d16c1-gcm:latest 为例：
```sh
cat <<EOF | sudo tee /opt/verdictd/image/policy.json
{
    "default": [{"type": "insecureAcceptAnything"}],
    "transports": {
        "docker": {
            "docker.io/zhouliang121/":
            [{"type": "insecureAcceptAnything"}]
        }
    }
}
EOF
```

4. 启动verdictd
```sh
verdictd --listen 0.0.0.0:20002 --verifier csv --attester nullattester --client-api 127.0.0.1:20001 --mutual
```
当Verdictd启动后，Verdictd在端口监听地址0.0.0.0:20002监听来自attestation agent的远程证明请求。

# 步骤七：部署加密镜像
## 1. 创建RuntimeClass对象kata
用户可以使用RuntimeClass为pod指定不同的运行时，这里使用kata作为验证时使用的运行时。

在集群中执行以下命令，创建RuntimeClass对象kata。
```shell
cat <<-EOF | kubectl apply -f -
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: kata
handler: kata
EOF
```
## 2. 部署pod
如果 pod 的 runtimeClassName 设置为 kata，CRI 插件会使用 Kata Containers 运行时运行 pod。
执行以下命令，部署名称为alpine的pod。
```shell
cat <<-EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: nginx-sandbox
spec:
  runtimeClassName: kata
  containers:
  - image: docker.io/zhouliang121/alpine-84688df7-2c0c-40fa-956b-29d8e74d16c1-gcm:latest
    command:
      - top
    imagePullPolicy: IfNotPresent
    name: alpine
  restartPolicy: Never
EOF

```
## 3. 测试加密镜像是否部署成功

执行以下命令，查看加密镜像是否部署成功：
```
kubectl get pods
```
预期输出：

![](https://oss.openanolis.cn/sig/txdolqfbikvgtxdsbpfw)

执行如下命令，查看加密镜像是否部署成功：
```shell
kubectl describe pod nginx-sandbox --namespace=default
```

verdictd预期输出：

![](https://oss.openanolis.cn/sig/bdzzruufvjybxjigmbqv)

从上述pod信息和virdictd输出信息可知已部署成功。

# 附录

## check_csv_env.sh内容

```sh
#!/bin/bash

# version: 1.0
# check the CSV confidential container verification env on Anolis8


#set -x

KERNEL_VERSION_MIN=5.10.
CSV_VERSION_MIN=1600



# check CPU model
check_cpu_model()
{
    echo -e "\n===========================\n"
    echo  -e "CHECK IF HYGON CPU: \n"
    echo  -e "lscpu: "
    lscpu

    is_hygon_cpu=`cat /proc/cpuinfo | grep Hygon`

    echo -ne "\ncheck if Hygon CPU: "
    if [ -n "$is_hygon_cpu" ];then
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    else
        echo -e "\tFaild!"
        echo -e "\tPlease use Hygon CPU to support CSV!\n"
        exit
    fi
}

# check kernel version
check_kernel_version()
{
    echo  -e "CHECK KERNEL VERSION: \n"
    uname_r=`uname -r`

    echo -e "\tcurrent version: ${uname_r}"

    echo -ne "\ncheck kernel version: "
    if [ "X${uname_r:0:5}" = "X${KERNEL_VERSION_MIN}" ];then
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    else
        echo -e "\tFaild!"
        echo -e "\tkernel version should be 5.10.x serious!\n"
        exit
    fi
}

#check CSV enabled
check_csv_enabled()
{
    echo -e "CHECK CSV ENABLED: \n"

    echo -e "dmesg | grep -i sev"
    dmesg | grep -i sev


    echo -en "\ncheck CSV enabled: "
    csv_support=`dmesg | grep -i "sev enabled"`
    if [ -z "$csv_support" ];then
        echo -e "\tFaild!"
        echo -e "\tPlease enable the CSV feature!\n"
        exit
    else
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    fi
}

#check CSV version
check_csv_version()
{
    set -e
    echo -e "CHECK CSV VERSION: \n"
    echo -e "sudo hag --platform_status"
    sudo hag --platform_status

    csv_plat_stat=`sudo hag --platform_status | grep "build id"`
    csv_version=${csv_plat_stat:0-4:4}
    echo -e "\n\tcurrent version: ${csv_version}"

    echo -ne "\ncheck CSV version: "
    if [ "$csv_version" -gt "$CSV_VERSION_MIN" ];then
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    else
        echo -e "\tFaild!"
        echo -e "\tThe CSV version should be greater than $CSV_VERSION_MIN!\n"
        exit
    fi
    set +e
}

#check firewalld stat
check_firewalld_stat()
{
    set -e
    echo -ne "CHECK FIREWALLD DISABLED: "

    firewalld_stat=`systemctl show -p ActiveState --value firewalld`
    if [ $firewalld_stat = "active" ];then
        echo -e "\tFaild!"
        echo -e "\tPlease disable the firewalld service!\n"
        exit
    else
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    fi
    set +e
}

#check selinux stat
check_selinux_stat()
{
    set -e
    echo -ne "CHECK SELINUX DISABLED: "

    selinux_stat=`getenforce`
    if [ $selinux_stat = "Enforcing" ];then
        echo -e  "\tFaild!"
        echo -e  "\tPlease disable or permissive the selinux!\n"
        exit
    else
        echo -e "\tPass!\n"
        echo -e "===========================\n"
    fi
    set +e
}

check_cpu_model
check_kernel_version
check_csv_enabled
check_csv_version
check_firewalld_stat
check_selinux_stat
echo -e "CSV base env all pass! Enjoy!\n"
```


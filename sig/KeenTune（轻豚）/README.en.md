# Introduction
KeenTune is a full-stack OS optimization product with AI and expert knowledge. It can provide lightweight and cross-platform performance tuning for mainstream opensource OS, to make the applications running with best performance in the AI customized OS environment.

# Projects and Reposities
### source repo:

https://gitee.com/anolis/keentuned  
https://gitee.com/anolis/keentune_bench  
https://gitee.com/anolis/keentune_target  
https://gitee.com/anolis/keentune_brain  


# Roadmap
2021.9.30 Apply Alinux and Anolis YUM
2021.10.21 KeenTune open source released
2021.12.30 KeenTune-UI open source
2022.3.30 Collaboration with external software and hardware vendors


# Mail List
keentune@lists.openanolis.cn

# Members
Owner：alex_yuxi
Maintainer：alex_yuxi、wangrunzhe、xifu_wql  
Contributor：lilinjie、lichao、liuzhilin、jiaominchao、 sj986461335、liutian

# KeenTune介绍
KeenTune（轻豚）是一款AI算法与专家知识库双轮驱动的操作系统全栈式智能优化产品，为主流的操作系统提供轻量化、跨平台的一键式性能调优，让应用在智能定制的运行环境发挥最优性能。


# KeenTune官网
http://keentune.io

# 项目仓库列表
注意：代码仓已经迁移至gitee，原codeup仓库不再维护！！！

### source repo:

https://gitee.com/anolis/keentuned  
https://gitee.com/anolis/keentune_bench  
https://gitee.com/anolis/keentune_target  
https://gitee.com/anolis/keentune_brain  
https://gitee.com/anolis/keentune_ui  


# 路线图
2021.9 正式开源，云栖官宣   
2021.12 Anolis8 YUM源   
2022.3 Anolis23 默认系统服务，集群管控版    
2022.6 KeenTune UI开源   
2022.9 Alinux3 YUM源，倚天定制版    
2022.12 KeenOpt开源


# 邮件列表
keentune@lists.openanolis.cn

# 成员
Owner：alex_yuxi  
Maintainer：alex_yuxi、wangrunzhe、xifu_wql、lilinjie  
Contributor：lichao、liuzhilin、sunqingwei、lichao、liuwenchao、jiaominchao、 sj986461335、liutian、tianweiweiliang、moviema、yuxiaojun、lvcongqing、chenjianglei、wuyong、zhangmei

# KeenTune钉钉交流群
钉钉群号：42497140    
二维码：
![](https://oss.openanolis.cn/sig/jxprrsawpjeyubgcwbed)

## SIG管理员
### Owner：
      陈莉君老师（西安邮电大学教授）
### Maintainer:
      毛文安：阿里云
      许庆伟：深信服
      赵亚雄：NewRelic
      陈恒奇：腾讯
      王璞：达坦科技
      汝磊：谐云
      
      
# 项目
## coolbpf
新开源的项目，用户通过远程编译节省资源开销，通过kernel module方式支持低版本3.10内核使用BPF。它的主要功能包括：
1. 本地编译服务，基础库封装，客户使用本地容器镜像编译程序，调用封装的通用函数库简化程序编写和数据处理；
2. 远程编译服务，接收bpf.c，生成bpf.so或bpf.o，提供给高级语言进行加载，用户只专注自己的功能开发，不用关心底层库安装、环境搭建；
3. 高版本特性通过kernel module方式补齐到低版本，如ring buffer特性，backport BPF特性到3.10内核；
4. BTF的自动生成和全网最新内核版本爬虫；自动发现最新的centos，ubuntu，Anolis等内核版本，自动生成对应的BTF；
5. 各内核版本功能测试自动化，工具编写后自动进行安装测试，保障用户功能在生产环境运行前预测试；
6. python，c，rust等高级语言支持。

代码库地址：
https://gitee.com/anolis/coolbpf
https://github.com/aliyun/coolbpf

## LMP
LMP 则是孵化于高校，通过构建机器学习模型等方案，从可视化平台的角度来进一步基于 eBPF 技术深入调试内核。
仓库地址：
https://gitee.com/linuxkerneltravel/lmp

## Eunomia

SIG新孵化的项目: 使用 WASM 或 JSON 的 eBPF动态加载开发框架,包含一个工具链和一个运行时，主要功能包括：

1. 不需要再为每个 eBPF 工具编写用户态代码框架：大多数情况下只需要编写内核态应用程序，即可实现正确加载运行 eBPF 程序，自动获取内核态导出信息；
2. 提供基于 async Rust 的 Prometheus 或 OpenTelemetry 自定义可观测性数据收集器，通常仅占用不到1%的资源开销，编写内核态代码和 yaml 配置文件即可实现 eBPF 信息可视化，编译后可在其他机器上通过 API 请求直接部署；
3. 将eBPF程序编译为 WASM 模块，通过 WASM 模块分发、运行、部署任意的用户态和内核态的 eBPF 程序，在其他平台上不需要重新编译 eBPF 工具即可启动
4. 拥有非常小和简单的可执行程序, 库本身小于 1MB 且不依赖 LLVM/Clang，也可以轻松嵌入到您的项目中，以小于 100ms 的时间动态加载和运行任何eBPF程序
5. 一行命令实现云端 eBPF 工具拉到本地部署运行，本地无需任何编译环境，不受内核版本和指令集架构的限制；

代码仓库地址：
https://gitee.com/anolis/eunomia https://github.com/eunomia-bpf/eunomia-bpf

可以通过 bolipi 平台一键体验（需要登录）：
https://bolipi.com/ebpf/home/online

## 邮件列表
os@lists.openanolis.cn

### 说明
eBPF技术探索SIG致力于深入研究分析BPF前沿技术和产学届进展，增强Networking、Tracing、Observability、Security四个纬度技术深度和落地效果，打造国内最有影响力的BPF项目和社区。
目前eBPF技术探索SIG邀请到国内最早Linux领域的奠基人、eBPF技术的领导者陈莉君老师担任这个SIG的Owner，管理员还有来自其他大厂同行，共同为国内eBPF技术的推广和发展而努力，带领我们开拓新的机会和可能。
## 钉钉群：

![](https://oss.openanolis.cn/sig/wlwgbufwloadejodewpr)

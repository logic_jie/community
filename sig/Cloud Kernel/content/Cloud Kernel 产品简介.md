# Anolis OS：修改文档

龙蜥操作系统([Anolis OS](https://openanolis.cn/anolisos))是龙蜥社区([OpenAnolis](https://openanolis.cn/))推出的完全开源、中立、开放的Linux发行版，它支持多计算架构，面向云端场景优化。Anolis OS 旨在为广大开发者和运维人员提供稳定、高性能、安全、可靠、开源的操作系统服务。

龙蜥操作系统内核(Anolis Cloud Kernel)是龙蜥社区维护版 Linux 内核, 源于 [Linux 社区主线稳定版本 LTS 内核](https://www.kernel.org/), 并回合了大量高版本内核的新特性, 新硬件支持, 稳定性增强及性能优化等补丁。致力于打造更稳定, 更可靠, 更安全, 特性更丰富, 性能更优化的 Linux 系统内核, 为 Anolis OS 以及下游生态合作 OS 发行版提供坚实的内核支持, 更好的支撑和扩展上层业务应用。

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/59656464/1656645847291-7f89d669-301e-4e52-b7b9-aad6bfabc221.png)

# Quick start：

* 如何订阅Anolis maillist

[https://openanolis.cn/sig/Cloud-Kernel/doc/607613103206853741](https://openanolis.cn/sig/Cloud-Kernel/doc/607613103206853741)

* 如何搭建Anolis OS环境:

[https://openanolis.cn/sig/Cloud-Kernel/doc/607571974230928479](https://openanolis.cn/sig/Cloud-Kernel/doc/607571974230928479)

* 如何下载，编译，和安装内核：

[https://openanolis.cn/sig/Cloud-Kernel/doc/607587039726485317](https://openanolis.cn/sig/Cloud-Kernel/doc/607587039726485317)

* 内核RPM打包教程：

[https://openanolis.cn/sig/Cloud-Kernel/doc/607593708787100770](https://openanolis.cn/sig/Cloud-Kernel/doc/607593708787100770)

* 如何向cloud kernel贡献您的代码：

[https://openanolis.cn/sig/Cloud-Kernel/doc/607596680293474815](https://openanolis.cn/sig/Cloud-Kernel/doc/607596680293474815)

* 报告问题：

您有任何有关龙蜥操作系统内核的问题, 缺陷, 特性, 需求等, 都欢迎您报告给我们。您可以按照以下教程报告一个问题.

[https://openanolis.cn/sig/Cloud-Kernel/doc/607601736106142822](https://openanolis.cn/sig/Cloud-Kernel/doc/607601736106142822)

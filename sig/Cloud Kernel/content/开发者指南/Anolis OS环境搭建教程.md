# 写在前面

Anolis OS 是 OpenAnolis 社区推出的完全开源、中立、开放的发行版，它支持多计算架构，也面向云端场景优化。

在您使用Anolis OS之前，我们提供了一个预装Anolis OS的在线机器资源服务。我们**强烈建议**您访问[**龙蜥实验室**](https://lab.openanolis.cn/#/apply/home)，使用Web页面及机器人等形式自动创建和管理机器资源，以此来对Anolis OS进行体验。

您可以访问[龙蜥实验室使用指南](https://www.yuque.com/anolis-docs/community/peng85)，来进行**一键申请**和 **免费试用** 。

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/63156315/1656644119956-01a1cabe-eb42-4c64-82d8-902d01afb26d.png)

我们提供两种方式安装Anolis OS：

* ISO镜像安装
* qcow虚拟机镜像安装

## 一、通过ISO进行安装

### 1.1 ISO镜像下载

登陆下载界面获取Anolis OS最新iso镜像文件

[https://openanolis.cn/download](https://openanolis.cn/download)

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/63156315/1656504756808-2cdce132-2ff8-4d66-a96d-18cd6525601a.png)

### 1.2 镜像安装

参考该文档，通过图形化安装接口部署Anolis8/7至目标平台：

[https://www.yuque.com/anolis-docs/manual/installation](https://www.yuque.com/anolis-docs/manual/installation)

## 二、 通过qcow虚拟机镜像安装

首先，验证CPU是否支持KVM；

`egrep '(vmx|svm)' /proc/cpuinfo`

如果结果中有vmx（Intel）或svm(AMD)字样，就说明CPU是支持的。

如果您是买的ECS，或者已经开了虚拟机，那大概率没办法再通过KVM的方式进行安装。

### 2.1 虚拟机镜像下载

登陆下载界面获取Anolis OS最新qcow2镜像文件

[https://openanolis.cn/download](https://openanolis.cn/download)

这里以7.9为例：点击网址中的下载按钮后，选择相应架构的文件夹进入，既可以看到对应的下载列表，请选择**AnolisOS-7.9-GA-x86_64-ANCK.qcow2**文件进行下载。

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/63156315/1656574325523-958feb42-fbbf-4974-9bd9-9d6827dd99db.png)

### 2.2 安装依赖包

`sudo yum install -y qemu-kvm libvirt virt-install bridge-utils`

### 2.3 启动前配置

#### 2.3.1 libvirt服务

开启libvirt服务

`systemctl start libvirtd`

设置开机启动

`systemctl enable libvirtd`

查看状态操作结果

`systemctl status libvirtd`

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/63156315/1656557798218-65ab7a31-63e2-4200-bca5-2ed9f65a169e.png)

`systemctl is-enabled libvirtd`

![](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/63156315/1656557863822-cec7389f-3748-43c9-8878-e03fe5906f4f.png)

#### 2.3.2 打开虚拟化的网络支持

`sudo virsh net-autostart default`

`sudo virsh net-start default`

`sudo sysctl -w net.ipv4.ip_forward=1` # 也可以写到配置文件里持久化

**TIPS：**

`sudo virsh net-autostart default` 执行过程中可能会卡住，此时将 `/etc/modprobe.d/blacklist.conf` 文件中的 "blacklist nf_conntrack_ipv4" 语句注释掉，例如

```
...
#blacklist nf_conntrack_ipv4
```

之后再执行 `sudo virsh net-autostart default`

#### 2.3.3 修改kvm权限

直接设置成root启动

```
cat >> /etc/libvirt/qemu.conf << EOF
user = "root"
group = "root"
EOF
systemctl restart libvirtd.service
```

#### 2.3.4 建立链接

查看qemu-kvm路径

`whereis qemu-kvm`

```
qemu-kvm: /etc/qemu-kvm /usr/libexec/qemu-kvm /usr/share/qemu-kvm /usr/share/man/man1/qemu-kvm.1.gz
```

建立软连接

`ln -s /usr/libexec/qemu-kvm /usr/bin/qemu-kvm`

#### 2.3.5 创建xml配置文件

示例文件的名称为anolis.xml，请根据提示修改您的镜像路径

您可以按照注释自己酌情修改。

```
<domain type='kvm'>
    <name>anolis</name> <!-- guest 名字 -->
    <memory>16777216</memory> <!-- 16GB 内存 -->
    <vcpu placement='static' cpuset='0-23' current='8'>8</vcpu> <!-- 8 core -->
    <cpu mode='host-passthrough'>
        <topology sockets='2' cores='2' threads='2'/>
    </cpu>
    <os>
        <type arch='x86_64' machine='pc'>hvm</type>
        <boot dev='hd'/>
    </os>
    <features>
        <acpi/>
        <pae/>
    </features>
    <clock offset="localtime" timezone="Asia/Shanghai">
        <timer name='rtc' tickpolicy='catchup' track='guest'/>
        <timer name='pit' tickpolicy='delay'/>
        <timer name='hpet' present='no'/>
    </clock>
    <on_poweroff>destroy</on_poweroff>
    <on_reboot>restart</on_reboot>
    <on_crash>restart</on_crash>

    <devices>
        <emulator>/usr/bin/qemu-kvm</emulator>
        <disk type='file' device='disk'>
            <driver name='qemu' type='qcow2' cache='none' dataplane='on' io='native'/> <!-- 如果要打 qcow2 快照，需要关闭 dataplane -->
            <source file='/root/workspace/AnolisOS-7.9-GA-x86_64-ANCK.qcow2'/> <!-- 填写您的系统镜像的绝对路径 -->
            <target dev='vda' bus='virtio'/>
        </disk>
        <!-- 这里可以如法炮制加入一些数据盘 -->
        <interface type='network'>
            <source network='default'/>
            <model type='virtio'/>
        </interface>
        <serial type='pty'>
            <target port='0'/>
        </serial>
        <console type='pty'>
            <target type='serial' port='0'/>
        </console>
        <serial type="tcp">
            <source mode="bind" host="0.0.0.0" service="33333"/>
            <protocol type="telnet"/>
        </serial>
        <video>
            <model type='cirrus' vram='9216' heads='1'/>
            <alias name='video0'/>
        </video>
        <input type='tablet' bus='usb'/>
        <input type='mouse' bus='ps2'/>
        <graphics type='vnc' port='-1' autoport='yes'/>
        <memballoon model='none'/>
    </devices>
</domain>
```

### 2.4 虚拟机的启动与管理

#### 2.4.1 使用virsh命令启动虚拟机

新机器执行virsh命令可能会有setlocale: No such file or directory的警告，可安装语言包

`yum install -y glibc-langpack-zh`

`sudo virsh define 虚拟机名.xml`

`sudo virsh start 虚拟机名` # 请修改为KVM虚拟机的真实名称。

vm 默认的账户和密码为：

* 用户名：`anuser`
* 密码：`anolisos`

#### 2.4.2 切换root用户并允许ssh root登录

1. `sudo su`
2. 输入密码`anolisos`
3. 修改root密码：`passwd root`
4. 修改`/etc/ssh/sshd_config`:

```
PasswordAuthentication yes

PermitRootLogin yes
```

#### 2.4.3 虚拟机的访问

可以通过下列方式访问VM:

* 通过 vnc 访问宿主机的 IP，登录VM，查看 IP
* 通过 `sudo virsh console 虚拟机名` 登录 VM（请注意可能会有一段时间的黑屏，VM启动过程没有输出到屏幕），查看 IP
* 获取到 Guest IP 之后，通过 `ssh root@<ip>` 登录 VM.

#### 2.4.3 查询虚拟机在宿主机对应串口设备

`virsh ttyconsole 虚拟机名`

#### 2.4.4 其余virsh命令

`virsh list` #显示本地活动虚拟机

`virsh list –-all<span> </span>` #显示本地所有的虚拟机（活动的+不活动的）

`virsh define 虚拟机名.xml` #通过配置文件定义一个虚拟机（这个虚拟机还不是活动的）

`virsh undefine 虚拟机名.xml` #删除虚拟机配置

`virsh start 虚拟机名` #启动名字为ubuntu的非活动虚拟机

`virsh create 虚拟机名.xml<span> </span>` # 创建虚拟机（创建后，虚拟机立即执行，成为活动主机）

`virsh suspend 虚拟机名` # 暂停虚拟机

`virsh resume 虚拟机名<span> </span>` # 启动暂停的虚拟机

`virsh shutdown 虚拟机名` # 正常关闭虚拟机

`virsh destroy 虚拟机名` # 强制关闭虚拟机

`virsh dominfo 虚拟机名` #显示虚拟机的基本信息

`virsh domname 2` # 显示id号为2的虚拟机名

`virsh domid 虚拟机名` # 显示虚拟机id号

`virsh domuuid 虚拟机名` # 显示虚拟机的uuid

`virsh domstate 虚拟机名` # 显示虚拟机的当前状态

`virsh dumpxml 虚拟机名` # 显示虚拟机的当前配置文件（可能和定义虚拟机时的配置不同，因为当虚拟机启动时，需要给虚拟机分配id号、uuid、vnc端口号等等）

`virsh setmem 虚拟机名 512000` #给不活动虚拟机设置内存大小

`virsh setvcpus 虚拟机名 4` # 给不活动虚拟机设置cpu个数

`virsh edit 虚拟机名` # 编辑配置文件（一般是在刚定义完虚拟机之后）

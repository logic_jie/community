from arm-sig: https://openanolis.cn/sig/ARM_ARCH_SIG/doc/657742613244594693

一、测试总结

针对龙蜥 OS MPAM 特性的整体测试情况如下：

经过对 MPAM 的功能性验证，目前 L3 cache 资源隔离和监控功能均正常，内存带宽隔离效果甚微，监控功能可用。
测试用例覆盖 MPAM 接口读写测试、并发压力测试等多种类型，测试结果未发现问题。
针对 MPAM ESR 中断，验证了 PARTID、PMG、monitor 相关异常能否触发中断、告知错误类型，中断监测功能正常可用。
二、MPAM 功能验证
2.1 cache 隔离功能验证
2.1.1 不同配置对实际 cache 占有量的影响

L3 cache 资源隔离以 ways 的方式进行配置。倚天机器共有 16 ways，测试对不同 ways 的隔离效果进行了验证。

numactl -m 0 -C 16 memhog -r10000000 100000m > /mnt/log

程序和资源隔离 group 绑定分别采用了 pid 绑定（tasks）和 cpu 绑定（cpus）两种方式。通过 schemata 接口设置程序所能够使用的 ways 数目，通过 mon_data/mon_L3_0*/llc_occupancy 接口读取程序的 L3 cache 占用。多次读取取平均值，并与理想的 cache ways 大小进行对比。

测试结果显示，L3 cache 隔离功能效果显著，无论是通过 tasks 绑定还是 cpus 绑定，均可以得到与理想值相接近的隔离效果。

2.1.2 不同配置对 mem latency 的影响

latency 作为一个重要的性能指标，在一些对时延敏感的场景来说，有很重要的参考作用，此处使用 lat_mem_rd 测试 cache 在不同的 ways 下，内存 latency 的分布情况，也从侧面验证 MPAM 对 cache 的隔离功能。

#设置步长为512字节
numactl -C 10 -m 0 ./lat_mem_rd -N 1 -P 1 145M 512

测试结果显示，随着 cache way 数目的增加，加载相同内存的 latency 逐渐减小。

2.1.3 L3 cache 抗干扰测试
# workload
numactl -m 1 -C 64-127 memhog -r10000000 100000m > /mnt/log
# distractor
numactl -m 1 -C 64-127 memhog -r10000000 100000m > /mnt/log

workload：保持 L3:1=fff0 配置无变化

distractor: 测试 L3:1=<mask>，mask 值分别为 0-f（无干扰）、0010-fff0（有干扰）

测试结果显示：

在无干扰情况下，workload 的 L3 cache 占用量基本无变化；

随着干扰 way 数逐渐变多，workload 和 distractor 两者的 L3 cache占比逐渐趋同，总量不变。

2.1.4 模拟混部 L3 cache 隔离测试

分别以 SPECjbb 2015 和 stress-ng 程序模拟在线环境和离线环境，对L3 cache隔离功能进行测试。两个环境均运行在 NUMA node 1 上。

在前 40s 的时间内，两个程序共享 L3 cache 资源。在约 40s 后，开始隔离在线和离线L3 cache资源的使用，在离线任务 L3 cache 的配比分别为 0xffff 和 0xf。

通过实验结果可以看到，在 L3 cache 资源共享的情况下，离线资源对在线资源干扰和压制明显，L3 cache 竞争激烈，波动幅度很大；在对 L3 cache 资源进行隔离后，一方面离线得到了持续有效的压制，L3 cache 占有率大幅下降，另一方面在线性能提升明显，而且波动幅度变小。

2.2 MB 隔离功能验证
2.2.1 不同配置对内存带宽的影响
gcc -O3 -fopenmp -DSTREAM_ARRAY_SIZE=100000000 -DNTIMES=1000 stream.c -o stream
# 单node测试
numactl -m 1 -C 64-127 ./stream
# 单CPU测试
numactl -m 1 -C 72 ./stream

MB 资源隔离以百分比的方式进行配置。测试以 5% 为粒度，通过设置 schemata 接口让内存带宽从 5% 逐次递增到 100%，通过读取 mon_data/mon_MB_0*/mbm_local_bytes 接口读取带宽值，最终取多次测量的平均值。

通过测试结果可以发现，不同百分比下的测试MB带宽值和100%带宽下的MB带宽值基本相等，倚天机器的 MB 带宽隔离效果甚微。

单 node（64 CPU） MB 配置结果

percent

	

stream测试值[Copy] (MB/s)

	

mbm_local_bytes接口值 (MB/s)




5

	

104808.5

	

104800.0




10

	

105028.3

	

105730.5




20

	

104459.3

	

104915.1




40

	

105077.6

	

105852.0




60

	

104980.6

	

105178.7




80

	

104924.8

	

105182.8




100

	

104828.1

	

105855.8

单 CPU MB 配置结果

percent

	

stream测试值[Copy] (MB/s)

	

mbm_local_bytes接口值 (MB/s)




5

	

25948.7

	

24147.7




10

	

25934.0

	

24433.1




20

	

25913.5

	

22771.2




40

	

25897.9

	

24559.4




60

	

25952.9

	

24079.7




80

	

25866.5

	

24246.4




100

	

25952.1

	

24171.9

三、MPAM稳定性测试
3.1 resctrl mount/umount

测试方法

挂载 resctrl 文件系统，设置 schemata 资源隔离接口，随机写 cpus/cpus_list、tasks 接口，读取mon_data 资源监控接口，最后卸载 resctrl 文件系统。重复 1000000 次。

测试结果

resctrl 文件系统相关接口仍可正常使用。

3.2 接口写入测试
3.2.1 schemata 写入

测试方法

创建两个 group，生成随机 L3 cache mask 和 MB 内存带宽值，并分别写入两个 group 的 schemata 接口，之后读取 schemata 接口，验证当前值是否与写入值相同。重复测试 1000000 次。

测试结果

schemata 均可正常写入。

3.2.2 schemata 错误写入

对 schemata 接口写入多种错误参数，验证 schemata 是否可以正确识别处理。

验证的错误类型及验证结果如下：

错误写入示例

	

last_cmd_status输出

	

测试结果




L3:0=10000

	

Mask out of range

	

PASS




L3:2=ff;3=ff

	

Unknown domain

	

PASS




L3

	

Missing ':'

	

PASS




L3:

	

Missing 'L3' value

	

PASS




L3:0

	

Missing '=' or non-numeric domain

	

PASS




L30:0=fff

	

Unknown or unsupported resource name 'L30'

	

PASS




L3:0=fghi

	

Non-hex character in the mask fghi

	

PASS




L3:1=ff;1=f4

	

Duplicate domain 1

	

PASS




MB:0=150

	

MB value 150 out of range 5-100

	

PASS




MB:0=4

	

MB value 4 out of range 5-100

	

PASS




MB:0=FOO

	

Non-decimal digit in MB

	

PASS




MB

	

Missing ':'

	

PASS




MB:0

	

Missing 'MB' value

	

PASS




MB:2=55

	

Unknown domain

	

PASS




MB:1=23;1=56

	

Duplicate domain 1

	

PASS




L3:0=ff (with cdp)

	

Unknown or unsupported resource name 'L3'

	

PASS

3.2.3 cpus/cpus_list 写入

测试方法

随机写入 cpus/cpus_list 接口 1000000 次，验证是否写入成功，并且 cpus 接口和 cpus_list 接口的值是否相对应。

测试结果

cpus/cpus_list 均可正常写入，并保持值的相等。

3.2.4 cpus/cpus_list 错误写入

错误写入示例

	

last_cmd_status输出

	

测试结果




echo 156 > cpus_list

	

Can only assign online CPUs

	

PASS




echo 4096 > cpus_list

	

Bad CPU list/mask

	

PASS




echo ffff > cpus_list

	

Bad CPU list/mask

	

PASS




echo 3-12 > cpus

	

Bad CPU list/mask

	

PASS

3.2.5 tasks 写入

测试方法

创建 500 个进程，并将其 pid 写入 tasks 接口，验证进程对应 pid 是否存在。之后 kill 掉所有进程，验证其 pid 是否已从 tasks 文件中移除。重复 1000000 次。

测试结果

tasks 接口均可正常写入和移除。

3.2.6 tasks 错误写入

错误示例

	

stderr

	

测试结果




将不存在pid写入tasks

	

echo: write error: No such process

	

PASS




echo hello > tasks

	

echo: write error: Invalid argument

	

PASS

3.2.7 mode 写入

mode 接口默认值为 shareable，当前MPAM接口暂不支持 mode 接口值的修改。

Mode

	

支持情况




shareable

	

支持




exclusive

	

不支持




pseudo-locksetup

	

不支持




pseudo-locked

	

不支持

3.3 group mkdir/rmdir 测试
3.3.1 max group 创建

测试方法

以 info/*/num_closids 为基准，创建所能达到的最多 group。重复 1000000 次。

测试结果

倚天 PARTID 数目为 64 个，因此除了 default group 外，最多能够创建 63 个 group。一般情况下均可达到最大值。但在部分 group 被使用过的情况下，由于其对应的 PARTID 在 L3 cache中占用量可能超过 /sys/fs/resctrl/info/L3_MON/max_threshold_occupancy，从而导致该 PARTID 在一定时间内不可用。

3.3.2 group 随机创建/删除

测试方法

随机创建/删除 group 共计 2000*(num_closids-1)，验证 group 分配和回收功能是否正常。

测试结果

group 随机创建和删除，group 分配/回收接口仍可正常运作。

3.3.3 mon_group 创建/删除

当前社区版本MPAM代码下 num_rmids 均为 1，暂不支持 mon_groups 目录下 mon group 的创建和删除。

3.4 并发读写测试
3.4.1 L3 cache 监控接口并发读取

测试方法

创建 5 个 group，每个 group 中写入 10 个进程：memhog -r1000000000 1m > /mnt/log

同时 10 个进程并发读 mon_data/L3_MON/llc_occupancy，持续时间 60 min。

测试结果

测试过程中未出现resctrl接口崩溃或不可用问题。

3.4.2 MB 监控接口并发读取

测试方法

创建 5 个 group，每个 group 中写入 10 个进程：memhog -r1000000000 1m > /mnt/log

同时 10 个进程并发读 mon_data/mon_MB_*/mbm_local_bytes，持续时间 60 min。

测试结果

测试过程中未出现 resctrl 接口崩溃或不可用问题。

3.4.3 schemata 接口并发写入

测试方法

创建 5 个 group，每个 group 中写入 10 个进程：memhog -r1000000000 1m > /mnt/log

同时 10 个进程并发随机写入 schemata，持续时间 60 min。

测试结果

测试过程中未出现 resctrl 接口崩溃或不可用问题。

3.4.4 cpus/cpus_list 接口并发写入

测试方法

创建 1 个 group，10 个进程并发写入随机 cpus/cpus_list，持续时间 60 min。

测试结果

测试过程未出现接口崩溃或不可用问题。

3.4.5 tasks 接口并发写入

测试方法

创建 1 个 group，10 个进程并发创建 300 个 task 并写入 tasks 接口。

测试结果

测试过程未出现接口崩溃或不可用问题。

四、MPAM 错误中断验证
4.1 L3 cache 资源错误中断验证

错误码

	

描述

	

结果

	

备注




0

	

No error captured in MPAMF_ESR

	

无

	

非异常情况




1

	

MPAMCFG_PART_SEL out of range

	

可触发

	







2

	

Request PARTID out of range

	

可触发

	







3

	

MSMON out of range PARTID/PMG

	

可触发

	







4

	

Request PMG out of range

	

不可触发

	

PMG>1时无法写入




5

	

MSMON_CFG_MON_SEL out of range

	

可触发

	







6

	

MPAMCFG_INTPARTID out of range

	

未测试

	

暂不支持PARTID narrowing




7

	

INTERNAL unexpected

	

未测试

	

暂不支持PARTID narrowing




8

	

MPAMCFG_PART_SEL.RIS unimplemented

	

不可触发

	

RIS>1时无法写入




9

	

MPAMCFG_PART_SEL.RIS no control

	

不可触发

	

RIS>1时无法写入




10

	

MSMON_CFG_MON_SEL.RIS unimplemented

	

不可触发

	

RIS>1时无法写入




11

	

MSMON_CFG_MON_SEL.RIS no monitor

	

不可触发

	

RIS>1时无法写入




12:18

	

Reserved
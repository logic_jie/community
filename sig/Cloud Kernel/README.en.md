Cloud Kernel is a customized and optimized version of Linux kernel. In Cloud Kernel, several features and enhancements adapted to specific cloud infrastructre and products have been made to help customers in cloud platform achieve better user experiences.

In 2020, Cloud Kernel joined OpenAnolis community, an open source operating system community and innovation platform.

Like many other kernels, Cloud Kernel should work with almost all commonly-used Linux distributions.

### DingTalk

![](https://oss.openanolis.cn/sig/lwsqmrxcdvxxadzhwpwi)

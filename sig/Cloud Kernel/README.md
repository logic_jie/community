云内核 (Cloud Kernel) 是一款定制优化版的内核产品，在 Cloud Kernel 中实现了若干针对云基础设施和产品而优化的特性和改进功能，旨在提高云端和云下客户的使用体验。

在 2020 年，云内核项目加入 OpenAnolis 社区大家庭，OpenAnolis 是一个开源操作系统社区及系统软件创新平台，致力于通过开放的社区合作，推动软硬件及应用生态繁荣发展，共同构建云计算系统技术底座。

与其他 Linux 内核产品类似，Cloud Kernel 理论上可以运行于几乎所有常见的 Linux 发行版中。

### 钉钉交流群

![](https://oss.openanolis.cn/sig/wbjudcwhgznycmirlsqf)
123

# 1 迁移注意事项

Anolis OS 7生态上和依赖管理上保持跟CentOS7.x兼容，一键式迁移脚本centos2anolis.py，实现CentOS7.x到Anolis OS 7的平滑迁移。

使用迁移脚本前需要注意如下事项： 

* 迁移涉及到软件包的重新安装，是不可逆过程，执行迁移前务必做好系统备份。
* 迁移过程涉及到访问Anolis OS的官方repo，需要确保待迁移环境网络能够正常访问Anolis OS repo。
* 需要使用root用户执行。
* 迁移过程依赖于yum，确保yum组件能够正常运行。
* 迁移脚本提供了Anolis OS repo访问加速的功能，如果访问Anolis OS官方repo速度较慢，可以通过-s选项进行加速访问。
* Anolis OS 不提供i686架构的软件包，如您系统上安装了CentOS的i686架构的软件包，则无法正常迁移。
# 2 部署迁移工具
下载迁移脚本：
```
wget https://mirrors.openanolis.cn/migration/anolis-migration.repo -O /etc/yum.repos.d/anolis-migration.repo
```
安装迁移脚本运行依赖：
```
yum -y install centos2anolis
```
3 迁移执行
```
centos2anolis.py
```
迁移完成后，系统会提示如下信息，表示迁移成功，重启即可进入Anolis OS系统。

![alt text](https://oss.openanolis.cn/fragment/dziozbchprwncwzhfyhw)

centos2anolis.py提供了-V选项，用于记录迁移前后的软件包信息，您可以根据需要判断是否添加选项。添加-V选项，迁移完成后会在/var/tmp目录下生成迁移前后的rpm信息，命名格式为：
```
# 迁移前
$(hostname)-rpms-list-before.log
$(hostname)-rpms-verified-before.log
# 迁移后
$(hostname)-rpms-list-after.log
$(hostname)-rpms-verified-after.log
```
# 4 迁移后验证
查看OS版本：
```
# cat /etc/os-release
NAME="Anolis OS"
VERSION="7.9"
ID="anolis"
ID_LIKE="rhel fedora centos"
VERSION_ID="7.9"
PRETTY_NAME="Anolis OS 7.9"
ANSI_COLOR="0;31"
HOME_URL="https://openanolis.cn/"
BUG_REPORT_URL="https://bugs.openanolis.cn/"

CENTOS_MANTISBT_PROJECT="CentOS-7"
CENTOS_MANTISBT_PROJECT_VERSION="7"
REDHAT_SUPPORT_PRODUCT="centos"
REDHAT_SUPPORT_PRODUCT_VERSION="7"
```
#5 FAQ
1）就地迁移失败是否可回滚？

迁移工具不支持回滚，迁移失败无法恢复到迁移初始状态，迁移前务必做好系统备份。

2）迁移出现问题怎么办？

a. 先在<a href="https://www.yuque.com/anolis-docs/kbase" target="_blank">龙蜥社区知识库</a>查看是否有同样的问题。

b. 保存迁移工具执行报错信息，并提供初始系统信息（系统版本，内核，软件包列表等等），附上迁移日志文件/var/log/centos2anolis.log，及时与我们联系获取帮助。
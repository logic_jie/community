1 迁移原理与注意事项

CentOS7.x到Anolis OS 8，无论是内核，基础软件包，工具链都发生了较大的变化。迁移工具需要考虑这些变化带来的兼容性问题。OpenAnolis社区提供的迁移工具leapp包含了迁移评估，迁移实施，配置还原等步骤，用于实现CentOS7.x到Anolis OS 8的就地迁移。

1.1 迁移评估

leapp扫描待迁移系统，搜集内核，软件包，系统配置基础信息，同时与目标系统（Anolis OS）进行对比分析，对于不兼容项给出影响分析和解决方案。如内核角度给出Anolis OS 中不再支持的内核特性，硬件驱动；软件角度给出系统命令的变更项，提示用户适配业务程序。leapp的迁移评估报告会给出当前系统中所有可能影响到迁移的影响项目，当这些影响项目都被解决后，用户才能够继续做迁移实施。同时业务程序可根据评估报告中的兼容性提示来适配迁移业务程序。

1.2 迁移实施

leapp首先搜集当前的系统信息，记录需要在重启后恢复的配置（如selinux状态）。迁移实施过程中，工具首先按照当前系统安装的软件包列表，并根据CentOS7.x到Anolis OS 8的软件包映射关系，从Anolis OS repo上提前下载迁移所需要的软件包，并基于Anolis OS的软件包制作upgrade-initramfs，在下一次重启后，系统自动进入upgrade-initramfs，并触发所有软件包的就地升级。在所有的软件包就地升级完成后，自动重启进入系统配置还原阶段，待所有的信息都完成配置后，系统重启进入新的OS，完成OS的就地迁移。

2 部署迁移工具
选项一 从本地 yum 源安装迁移工具

1. 如果待迁移系统无法访问龙蜥 mirror，首先建议在内网搭建一套本地yum源。(如何做本地yum源 用户名： rsync_user ， 密码： Rsync@2020) ，然后通过本地源安装迁移工具。假设本地源地址为 http://local.repo.com/anolis ， 则下载迁移工具软件源：

wget http://local.repo.com/anolis/migration/anolis-migration.repo -O /etc/yum.repos.d/anolis-migration.repo

2. 然后执行下述命令将 /etc/yum.repos.d/anolis-migration.repo 里面的baseurl地址替换为本地源地址。

sed -i "s#baseurl=https://mirrors.openanolis.cn/#baseurl=http://local.repo.com/#" /etc/yum.repos.d/anolis-migration.repo
sed -i "s#gpgkey=https://mirrors.openanolis.cn/#gpgkey=http://local.repo.com/#" /etc/yum.repos.d/anolis-migration.repo

3. 安装迁移工具：

pip list | grep requests && pip uninstall requests urllib3 -y
yum -y install leapp
yum -y install python-urllib3 python-requests

备注：重新安装 python-requests 和 python-urllib3 是为了解决迁移过程中可能发生的软件包升级冲突。

4. 执行下述命令将 /etc/leapp/files/leapp_upgrade_repositories.repo 里面的baseurl地址替换为本地源地址。

sed -i "s#baseurl=https://mirrors.openanolis.cn/#baseurl=http://local.repo.com/#" /etc/leapp/files/leapp_upgrade_repositories.repo

选项二 从社区 yum 源（mirrors.openanolis.cn) 安装迁移工具

如果待迁移系统可以联网，则下载迁移工具软件源：

wget https://mirrors.openanolis.cn/anolis/migration/anolis-migration.repo -O /etc/yum.repos.d/anolis-migration.repo


安装迁移工具：

pip list | grep requests && pip uninstall requests urllib3 -y
yum -y install leapp
yum -y install python-urllib3 python-requests
3 迁移前评估

成功部署工具后，运行工具的评估命令来对系统环境进行扫描评估：

leapp preupgrade --no-rhsm

上述命令是默认评估迁移到带 ANCK 内核的龙蜥OS，如果你想评估迁移到带 RHCK 内核的龙蜥OS请执行下面的命令

leapp preupgrade --no-rhsm --disablerepo=anolis_plus
3.1 查看评估报告

评估命令执行完成后，会生成评估报告，在/var/log/leapp/leapp-report.txt中,报告中详细列举出当前OS与目标OS系统间的升级分析报告，每一个报告点都包含影响等级，影响范围以及解决方案。

当评估工具认为报告点影响到了OS的就地升级，这些报告点问题就必须解决，否则无法实施就地迁移。

报告样式如下，包含影响等级，影响概述，影响详细信息，推荐解决方案。

上述报告提示用户在Anolis OS中默认没有python命令，系统中默认提供的是不向前兼容的python3，并且python2只是有限支持，用户需要尽快将业务程序尽快迁移到使用python3。同时报告也给出了解决方案来使系统默认提供python命令。

3.2 基于评估报告处理迁移前问题

迁移评估报告中一旦遇到影响到就地迁移的因素，会禁止做迁移实施，将这些影响因素都解决后才能够继续实施迁移。如果遇到影响迁移实施的因素，迁移评估结果显示如下：

按照报告的提示，在/var/log/leapp/leapp-report.txt可以获取到详细的报告信息，同时UPGRADE INHIBITED给出的影响升级的因素也可以在/var/log/leapp/answerfile文件中查看。

# cat /var/log/leapp/answerfile
[remove_pam_pkcs11_module_check]
# Title:              None
# Reason:             Confirmation
# =================== remove_pam_pkcs11_module_check.confirm ==================
# Label:              Disable pam_pkcs11 module in PAM configuration? If no, the upgrade process will be interrupted.
# Description:        PAM module pam_pkcs11 is no longer available in Anolis 8 since it was replaced by SSSD.
# Type:               bool
# Default:            None
# Available choices: True/False
# Unanswered question. Uncomment the following line with your answer
# confirm =

解决方案，在新的Anolis OS 8中不再支持pam_pkcs11，所以我们直接选择删除该不支持的选项：

leapp answer --section remove_pam_pkcs11_module_check.confirm=True

Anolis OS 8中默认的PermitRootLogin行为是prohibit-password，禁止root用户登陆，如果不显式设置PermitRootLogin yes则会在迁移后影响root用户密码登陆。

解决方案：修改sshd_config 配置文件，允许root用户登录

sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/' /etc/ssh/sshd_config

解决了上述影响因素后，可以再次执行leapp preupgrade --no-rhsm或者 leapp preupgrade --no-rhsm --disablerepo=anolis_plus 再次评估是否有影响迁移的因素。若没有影响迁移的因素，结果如下：

3.2.4 迁移实施

解决了评估报告中所有影响就地迁移的问题后，即可实施迁移。

leapp upgrade --no-rhsm

上述命令是默认迁移到带 ANCK 内核的龙蜥OS，如果你想迁移到带 RHCK 内核的龙蜥OS请执行下面的命令

leapp upgrade --no-rhsm --disablerepo=anolis_plus

该步骤涉及到从目标系统（Anolis OS repo）上下载软件包用于就地升级，确保待迁移环境能够正常访问Anolis OS repo。

上述命令执行成功后，还可以通过/var/log/leapp/leapp-report.txt查看迁移报告，该报告除了包含评估报告外，还包含对目标系统repo的可行性评估，如果目标系统软件包不符合迁移要求，会给出提示。

以下图为例，Anolis OS 8中不再支持kde，评估报告中给出了这样的提示：

一切就绪，执行结果如下：

接下来需要执行  reboot  命令，reboot后OS进入一个隔离环境自动进行实质的迁移操作。

待系统迁移完成后，OS 会再次自动重启进入迁移后的系统。这一步骤涉及到2次OS重启以及相关的迁移操作故耗时较长。

3.2.5 迁移后验证

就地迁移完成后，进入新的操作系统，可以通过/var/log/leapp/leapp-report.txt查看迁移报告，报告中会包含就地迁移以及迁移后的系统详细报告信息，也可以通过/var/log/leapp/leapp-upgrade.txt查看就地迁移的执行日志。

查看报告/var/log/leapp/leapp-report.txt可以获取升级过程中的一些信息，比如因为某些原因软件包没有升级成功，需要手动处理这些软件包。

/var/log/leapp/leapp-upgrade.txt则记录了整个迁移过程，

迁移完成后，可通过一系列基础操作查看OS版本，如/etc/os-release查看OS版本。

迁移完成后，应用程序可以通过自身的指标查看是否能够正常运行。

4 FAQ

1）执行“leapp upgrade --no-rhsm”时出现因网络问题下载软件包失败，如何解决？

修改/etc/leapp/files/leapp_upgrade_repositories.repo文件，将mirrors.openanolis.cn替换为mirrors.aliyun.com

2)  待迁移环境需要配置代理访问外网，leapp运行过程中下载软件包报错，如何解决？

leapp执行过程中真正生效的repo是/etc/leapp/files/leapp_upgrade_repositories.repo，修改/etc/leapp/files/leapp_upgrade_repositories.repo 文件，为每个repo配置代理，

3）如何确认迁移前后软件包变化？

使用leapp进行OS迁移后，可通过迁移日志/var/log/leapp/leapp-upgrade.log查看软件包更新详细信息。

4）就地迁移失败是否可回滚？

迁移工具不支持回滚，迁移失败无法恢复到迁移初始状态，迁移前务必做好系统备份。

5）迁移出现问题怎么办？

迁移过程中出现任何问题，可保存当前迁移工具执行报错信息，并提供初始系统信息（系统版本，内核，软件包列表等等），及时与我们联系获取帮助。



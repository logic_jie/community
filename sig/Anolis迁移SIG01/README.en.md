# Group goals

Provide migration guidance and migration tools from CentOS to Anolis OS, users can use Anolis OS simply and efficiently.

1. Provide in-place migration tool from CentOS 8 to Anolis OS 8
2. Provide in-place migration tool from CentOS 7 to Anolis OS 7
3. Provide in-place upgrade tool from CentOS 7 to Anolis OS 8

# Ongoing project

1. OS Migration Tool

Provide CentOS 8 to Anolis OS 8 and CentOS 7 to Anolis OS 7 in-place migration tool, users can use the migration script to migrate to Anolis OS in place.

Open source: [https://gitee.com/anolis/centos2anolis](https://gitee.com/anolis/centos2anolis)

1. 7 to 8 migration tool

Provide CentOS 7 to Anolis OS 8 upgrade evaluation and in-place upgrade tool, users can use this tool to achieve CentOS7 to Anolis OS 8 in-place upgrade.

Open source:

* [https://gitee.com/src-anolis-sig/leapp](https://gitee.com/src-anolis-sig/leapp)
* [https://gitee.com/src-anolis-sig/leapp-repository](https://gitee.com/src-anolis-sig/leapp-repository)

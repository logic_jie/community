# 小组目标

提供从CentOS迁移到Anolis OS的迁移指导和迁移工具，用户能够简单高效的使用到Anolis OS。

1. 提供从CentOS 8迁移到Anolis OS 8的一键式迁移工具
2. 提供从CentOS 7迁移到Anolis OS 7的一键式迁移工具
3. 提供从CentOS 7升级到Anolis OS 8的一键式升级工具

# 正在进行的项目

1. OS迁移工具

提供CentOS 8到Anolis OS8，CentOS 7到Anolis OS 7一键式迁移工具，用户可使用迁移脚本就地迁移到Anolis OS。

已开源: [https://gitee.com/anolis/centos2anolis](https://gitee.com/anolis/centos2anolis)

1. 7迁8迁移工具

提供CentOS 7到Anolis OS 8的升级评估和就地升级工具，用户可使用该工具实现CentOS 7到Anolis OS 8的就地升级。

已开源：

* [https://gitee.com/src-anolis-sig/leapp](https://gitee.com/src-anolis-sig/leapp)
* [https://gitee.com/src-anolis-sig/leapp-repository](https://gitee.com/src-anolis-sig/leapp-repository)

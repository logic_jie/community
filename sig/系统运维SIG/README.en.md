# Group Goals
Based on the ANCK kernel of the OpenAnolis community and Anolis OS to build Intel x86/x86_64 architecture compatible kernel, OS and full-stack software ecosystem, supporting the next generation of chip platforms and functions of the Intel architecture.
The following is involved:
1. New Intel hardware, new platforms, and new technical support
    - Intel IceLake, Cooper lake and previous Xeon platform support
    - Intel Sapphire Rapids platform development and productization
    - Intel new technical support and ecosystem development
2. Full stack development, enablement, optimization and innovation
3. Solution rollout and best practice application
4. Combine OpenAnolis other SIG to facilitate the adoption, evolution, and sharing of new Intel technologies

# Communication Groups
### DingTalk Group

<img src="https://oss.openanolis.cn/sig/djuggfngdoptobsnbfoi" width = "430" height = "600" div align=center />

### WeChat Group

<img src="https://oss.openanolis.cn/sig/yxljchjbztxqcemozykw" width = "430" height = "600" div align=center />

# Associated SIG
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://oss.openanolis.cn/fragment/qtxqmajokpsvvkxhhyto?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/Cloud-Kernel" title="Cloud Kernel"> Cloud Kernel</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://oss.openanolis.cn/fragment/xgrdnqkbxrriuocxcigb?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/crypto" title="Full-stack ShangMi"> Full-stack ShangMi</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/AI_SIG" title="AI SIG"> AI SIG</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/pmem" title="Persistent Memory"> Persistent Memory</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/Virt" title="Virtualization"> Virtualization</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/coco" title="Cloud Native Confidential Computing"> Cloud Native Confidential Computing</a></div></td>

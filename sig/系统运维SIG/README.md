# 小组目标
基于龙蜥社区 ANCK 内核以及于 Anolis OS 构建 Intel x86/x86_64 架构兼容内核、OS 以及全栈软件生态，支持 Intel 架构的下一代芯片平台及功能。
主要包括：
1. Intel 新硬件、新平台和新技术支持
    - Intel IceLake, Cooper Lake 以及之前的至强平台支持
    - Intel Sapphire Rapids 平台研发和产品化支持
    - Intel 新技术支持和生态建设
2. 全栈开发、使能、优化和创新
3. 解决方案推广和最佳实践应用
4. 联合龙蜥关联 SIG 促进 Intel 新技术应用、演进和分享


# 交流群
### 钉钉交流群

<img src="https://oss.openanolis.cn/sig/djuggfngdoptobsnbfoi" width = "430" height = "600" div align=center />

### 微信交流群

<img src="https://oss.openanolis.cn/sig/yxljchjbztxqcemozykw" width = "430" height = "600" div align=center />

# 关联 SIG
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://oss.openanolis.cn/fragment/qtxqmajokpsvvkxhhyto?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/Cloud-Kernel" title="Cloud Kernel"> Cloud Kernel</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://oss.openanolis.cn/fragment/xgrdnqkbxrriuocxcigb?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/crypto" title="商密软件栈"> 商密软件栈</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/AI_SIG" title="人工智能兴趣小组"> 人工智能兴趣小组</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/pmem" title="持久内存"> 持久内存</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/Virt" title="虚拟化"> 虚拟化</a></div></td>
<td class="ant-table-cell" style="text-align: left;"><div class="table_name_box"><img class="table_name_img" src="https://openanolis.cn/assets/static/sig.png?x-oss-process=image/resize,h_40,w_40,m_fill,limit_0" width = "20" height = "20" div align=center /><a class="table_name_a" target="_black" href="https://openanolis.cn/sig/coco" title="云原生机密计算"> 云原生机密计算</a></div></td>

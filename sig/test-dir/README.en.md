## SIG Home

https://openanolis.cn/sig/t-one

## SIG Mission
T-One SIG focus on building a one station test system, to fully support OpenAnolis community test activities.

The main activities of this SIG are:
1. Explore excellent solutions, collect requirements by the community, design and plan direction on the test platform.
2. Develop and maint T-One/Testfarm to support testing tasks by the community.
3. Make T-One open-source, attract develpers by the community, provide testing services for community developers and cooperative enterprises.

## T-One Link
T-One：https://tone.openanolis.cn/

Testfarm：https://testfarm.openanolis.cn/

## Members
| Member  |  Role |
| ------------ | ------------ |
| [yongchao](https://gitee.com/zy_chao)  |  maintainer | 
| vosamowho  | maintainer | 
| wjn740  | maintainer | 
| suqingming  | maintainer | 
| jacob2021 | maintainer | 
| fuyong  | maintainer | 
| wenlylinux  | contributor |
| zhangxuefeng  | contributor |
| wb-cy860729  |  contributor |
| jpt2021  |  contributor |
| woohello |  contributor |
| as461177513 | contributor |
| vosamowho |  contributor |

## SIG Repositories

Source code repositories:
- https://gitee.com/anolis/testfarm
- https://gitee.com/anolis/testfarm-front
- https://gitee.com/anolis/tone-web
- https://gitee.com/anolis/tone-runner
- https://gitee.com/anolis/tone-agent
- https://gitee.com/anolis/tone-agent-proxy
- https://gitee.com/anolis/tone-agent-front
- https://gitee.com/anolis/tone-front
- https://gitee.com/anolis/tone-deploy
- https://gitee.com/anolis/tone-cli
- https://gitee.com/anolis/tone-storage


## Meetings

## Chat GROUP

欢迎使用钉钉扫码入群

![](assets/表情-07.png)
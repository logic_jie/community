# technical-committee



### 技术委员会职权：

（一）定义和维护社区技术发展的方向和蓝图；

（二）制定和监督社区SIG组的工作流程，指导社区SIG小组的开发；

（三）定制Linux发行版的技术路线，对系统架构、构建发布等进行指导；

（四）构建社区项目运行流程，保障社区的开放性和透明性，推动社区技术生态健康发展；

（五）协调跨SIG组及项目的合作，对社区跨项目技术问题进行指导；

（六）制定、指导项目孵化、开发、退出流程，接受用户及社区参与者的反馈（需求和问题），推动社区资源将其落地至项目；

（七）建立社区认证标准和平台，为社区认证（操作系统商业发行版认证、硬件兼容性认证等）提供技术支撑；



**决策机制：**

- 社区每间隔一月举行例会；
- 涉及事项决策的例会，出席人数应达成员二分之一以上方能召开（且其中必须包含主席），就各项决策事项应经出席人数的过半数同意通过；如果决策结果存在平票，主席将对该事项具有一票决策权。
- 会议具体议程和具体参会方式将至少提前三个工作日在社区邮件列表中通知。

### PMC（Project Management Commitee）

项目管理委员会，为项目核心管理团队，参与 Roadmap 和重大决议决策，从 committer 中产生

**如何成为 PMC：**

- 超过社区2⁄3 PMC 投票通过；
- 长期关注项目发展并深度参与项目讨论的 committer，成为 committer 至少半年时间；

**职责：**

- 积极参与项目讨论，对项目重大决策给予指导；
- 负责保证开源项目的社区活动都能运转良好；

**权利：**

- Pull Request review 权限；
- Pull Request approve 权限；
- Merge 权限；
- 对于项目重大决定的投票权；



### Maintainer

具有代码提交能力的开发者.

**如何成为 Committer：**

由已有的 PMC 推荐，并通过2⁄3以上投票通过，满足必须条件以及选择条件中的一个：

必须满足：

- 成为社区成员时间超过三个月；

以下三个条件任意满足一个即可：

- 超过10个 PR 合并；
- 完成至少一个重大功能；
- 修复至少一个严重 Bug；

**职责：**

- 社区咨询支持；
- 积极响应指派给您的 Issue 或 PR；
- Review 社区的 PR；

**权限：**

- Pull Request review 权限；
- Merge 权限；
- 获得 OpenAnolis Maintainer 勋章；



### Contributor

做过代码贡献的开发者。

**如何成为 Contributor：**

- 在 OpenAnolis的任何一个项目代码库中成功提交一个 PR 并合并。

**职责：**

- 积极响应指派给您的 Issue 或 PR；

**权限：**

- 加入 OpenAnolis GitHub/Gitee 组织，成为OpenAnolis开源社区的一员。
- 获得 OpenAnolis Contributor 小礼物；



### User Group（社区层面）

**1. Community Leader**

**如何成为 Community Leader：**

由已有的 PMC 推荐，参与 OpenAnolis 布道，必须满足以下两个以上条件：

- 成为社区成员时间超过4个月，并4个月内保持活跃；
- 原创 OpenAnolis 相关文章并发布数达到3篇以上；
- 至少代表 OpenAnolis，参与大会、Meetup 等分享一次；

**职责：**

- 社区咨询支持；
- 积极响应指派给您的文章或分享；
- 对于社区运行规则的投票权；

**权利：**

- 可以对社区运营方向建议以及推进；



\2. Ambassador

**如何成为 Ambassador：**

以下条件任意满足之一即可

- 原创 OpenAnolis 相关文章并成功发布一篇以上。
- 在社区活跃超过1个月，并答疑超过30多次。

**职责：**

- 积极响应社区内提问；

**权利：**

- 获得 OpenAnolis 相关周边。

\3. 城市站社区管理者

**如何成为城市站社区管理者：**

- 只要你对 OpenAnolis 有热情，愿意为 OpenAnolis 的布道贡献自己的一份力，一次及以上参与社区线下共建；

**职责：**

- 主导 OpenAnolis 城市站线上线下活动，包括但不限于 Meetup、Channel 等形式；
- 参与 OpenAnolis 城市布道；

**权利：**

- 认证成为 OpenAnolis 城市站社区管理者，获得相关证书；
- 获得 OpenAnolis 相关运营周边支持；
- 获得 OpenAnolis 运营以及内容支持；

## 联系我们

网站：[https://openanolis.cn](https://gitee.com/link?target=https%3A%2F%2Fopenanolis.cn)
钉钉交流群：33311793
微信公众号：OpenAnolis龙蜥
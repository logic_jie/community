# SIG流程指南
    SIG运行和管理
* 各SIG实行自治管理，按照多数票原则投票决策。
* 各SIG每隔半年向技术委员会做运行总结报告，TC给出相应的指导和帮助。
* 其他合并等变更有SIG 维护者向技术委员会提交申请，并获批后变更。

## 1 申请新SIG流程
### 1.1 使用模板填写信息
* 第一步：将 gitee.com/anolis/community/sig/sig-template Fork到你的Gitee下  
* 第二步：将sig-template复制一份并重新命名(目录名称以后都不允许修改，否则会导致数据同步失败，请谨慎命名)  
* 第三步：修改“重新命名目录”下的文件内容(SIG目录里定义了SIG元信息，这些信息作为社区基础设施和SIG同步程序的输入，以保证社区的自动化运作，所以在填写的时候需要遵守以下规范填写SIG信息)  

    * sig-info.yaml 文件要求  
    name：SIG中文名称，*必填。  
    en_name：SIG英文名称，*必填。  
    home_page：SIG主页的URL，*必填且不能和现有重复，格式：https://openanolis.cn/sig/+英文后缀， 后缀只允许包含字母、数字、下划线(_)、中划线(-)、英文句号(.)，必须以字母开头，且长度为2~191个字符 (如：https://openanolis.cn/sig/t-one)。  
    description：SIG中文简介，*必填。  
    en_description：SIG英文简介。  
    mailing_list：SIG的邮件列表。  
    meeting_url：SIG的固定会议纪要信息，一般是etherpad 的url。  
      
        maintainers：（对应SIG的maintainers）  
        openanolis_id：社区账户名称。  
        gitee_id：gitee帐户id。  
    
        contributors：（对应SIG的contributors）  
        openanolis_id：社区账户名称。  
        gitee_id：gitee帐户id。   
    
        repositories：（SIG相关的仓库信息）  
        仓库的填写格式：{group_name}/{repo_name}，如 anolis/community，多个仓库请填写多行。

    * README.md 文件要求  
    SIG中文介绍，内容不能为空可自定义填写，会作为SIG主页同步到社区官网对应的SIG主页（请不要将SIG maintainers、contributors填写在这里，以免跟sig-info.yaml中重复）

    * README.en.md 文件要求  
    SIG英文文介绍，填写规则与中文介绍相同。

    * content 文件夹  
    content文件夹用来存放SIG文档，content下的文件夹与文档，会在社区映射成目录用来访问。暂时只支持三级，超出层级内容不会被同步。文档格式暂时只支持(.md)文档，文档内图片请存放在assets文件夹下，以相对路径的方式引用。不支持其他图床上的地址。


    * assets 文件夹 
    资源类文件请存放在该文件夹下，同步程序会默认跳过。

### 1.2 提交PR
将以上修改提交到Gitee上，并在Gitee上创建一个Pull Request。同步程序会自动检查内容是否符合规范，如不符合规范请按提示修改。修改后可在Pull Request评论区，回复“/retest”指令重新检查，直到同步程序回复可以同步为止。如回复指令后评论区未看到提示，请刷新页面后再查看。

### 1.3 向TC发送邮件申请
发送邮件到技术委员会tc@lists.openanolis.org提交创建申请，并附带Pull Request地址，并进入TC委员会讨论。

### 1.4 TC评审
技术委员会将在社区会议上对SIG申请进行审批，审批后对PR进行合并或拒绝操作。
技术委员会指派一位委员帮助SIG的建立，包括协助相关资源，指导SIG的建立和初期运转。

### 1.5 TC评审通过
* 申请访问Buildsystem来构建rpms软件包
* 申请访问QA测试系统来测试软件包
* 申请SIG的Bugs管理权限

 

## 2 变更SIG流程
### 2.1 更新sig-info.yaml
更新sig文件夹下sig-info.yaml文件，规则请参考：“1.1 使用模板创建，第三步sig-info.yaml文件要求“，home_page字段请不要修改。

### 2.2 更新README.md
根据修改内容请同步更新README内容，规则请参考：“1.1 使用模板创建，第三步README.md文件要求“

### 2.3 更新README.en.md
根据修改内容请同步更新README.en内容，规则请参考：“1.1 使用模板创建，第三步README.en.md文件要求“

### 2.4 提交PR
将以上修改提交到Gitee上，并在Gitee上创建一个Pull Request，规则请参考：“1.2 提交PR”。

### 2.5 注意事项
存放sig文件夹名称请不要修改，否则会导致异常数据问题。  
sig-info.yaml里的home_page，做为sig社区访问唯一路径，也不允许修改，否则同步会失败。

## 3 变更SIG文档
### 3.1 修改content目录下文件
支持新增、修改、删除现有文件。目录层级最多只支持三级。规则请参考：“1.1 使用模板创建，第三步content文件夹”。 

### 3.2 提交PR
将以上修改提交到Gitee上，并在Gitee上创建一个Pull Request，规则请参考：“1.2 提交PR”。

## 4 SIG退出机制
TC委员会可依据以下原则，由技术委员提出并提交TC委员会会议，决议SIG的撤销：
* SIG的工作内容无法满足社区发行版或者社区技术发展方向。
* SIG长期没有活跃度，或者无成果产出。

## 5 FAQ
### 1 回复指令后未看到结果
目录文件缺失